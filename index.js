const fs = require('fs'),
prompt = require("prompt-sync")()

var updateTime = 45

var clientId = "1176317740512985119"

const steamID = process.env.STEAM_ID || prompt("What is your Steam ID?: ")

const thumborURL = "https://thumbor-production-0e82.up.railway.app/unsafe/fit-in/512x512/filters:fill(transparent):background_color(rgb(50,%2050,%2050))/"

if (updateTime < 30) { // Please, refrain from changing this! This program is already effectively DDoSing valve servers, I dont want it to go any further!
    console.error("Update time must be above 30! Please change your settings!")
    return
}

if (steamID == undefined) {
    console.error("No Steam ID was defined! Please check the documentation for how to add one.")
    return
}

var RPC = require("discord-rpc")

const client = new RPC.Client({ transport: 'websocket' })

client.login({ clientId })

var startTime = Date.now()

var prevGame = ""

function update() {
    fetch(`https://steamcommunity.com/miniprofile/${steamID}`)
        .then((data) => {
            return data.text()
        })
        .then((html) => {
            var json = {
                "name": "nothing",
                "presence": "",
                "img": "",
                "pfp": "",
                "username": ""
            }

            if (html.includes("game_name")) {
                var blah = html.substring(html.indexOf('game_name">') + 11, html.length)
                json["name"] = blah.substring(0, blah.indexOf("</span>"))
            }

            if (html.includes("presence")) {
                var blah = html.substring(html.indexOf('presence">') + 10, html.length)
                json["presence"] = blah.substring(0, blah.indexOf("</span>"))
            } else if (html.includes("game_state") && json.name != "nothing") {
                var blah = html.substring(html.indexOf('game_state">') + 12, html.length)
                json["presence"] = blah.substring(0, blah.indexOf("</span>"))
            }

            if (html.includes("persona")) {
                var blah = html.substring(html.indexOf("persona"), html.length)
                json.username = blah.substring(blah.indexOf(">") + 1, blah.indexOf("</span>"))
            }

            if (html.includes("game_logo")) {
                var blah = html.substring(html.indexOf('game_logo" src="') + 'game_logo" src="'.length + 8, html.indexOf("capsule"))
                json.img = thumborURL + blah + "logo.png"

                json.pfp = html.substring(html.indexOf('<img src="https://avatars') + 10, html.indexOf("medium")) + "full.jpg"
                
            }

            if (prevGame != json.name) {
                prevGame = json.name
                startTime = Date.now()
            }

            if (json.name == "nothing") {
                client.clearActivity()
                updateTime = (process.env.UPDATE_TIME || 45) * 2.5
            } else {
                client.setActivity({
                    details: `Playing ${json.name}`,
                    state: json.presence,
                    startTimestamp: startTime,
                    largeImageText: json.name,
                    largeImageKey: json.img,
                    smallImageText: json.username,
                    smallImageKey: json.pfp || null,
                });
                updateTime = (process.env.UPDATE_TIME || 45)
            }
        })
    setTimeout(() => {
        update()
    }, updateTime * 1000);
}

client.on('ready', () => {
    update()
});

process.on('uncaughtException', (err, origin) => {
    fs.writeSync(
        process.stderr.fd,
        `Caught exception: ${err}\n` +
        `Exception origin: ${origin}`,
    )
})
