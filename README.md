# SteamRPC

SteamRPC pulls your game and presence from Steam and puts it onto discord.

## Table of Contents
- [Setup](#setup)
    - [Downloading the repository](#downloading-the-repository)
    - [Steam Setup](#steam-setup)
    - [Running it](#running-it)
- [Notes](#notes)
- [To-Do](#to-do)

## Setup

### Downloading the repository
Simply run:
```bash
git clone https://codeberg.org/Bingus_Violet/SteamRPC
cd SteamRPC
```

In order for SteamRPC to run, you need to provide it with a Steam ID. There are 2 ways to do this, the first is through environment variables.
<br> Example:
```bash
STEAM_ID=12345678 node index.js
```
Alternatively, running SteamRPC without providing an ID through an environment variable will make it prompt you for one.

Below you can find documentation for how to get your Steam ID.

### Steam Setup

Getting your Steam ID is a much simpler process.
1. Navigate to Steam and login if you haven't already (You can do this both in the app or on the web)
2. Hover on your username at the top and go to "friends"
3. Navigate to the "Add a friend page" on the left
4. Copy your friend code

Now replace the STEAM_ID environment variable with the SteamID you just got.

Although this isn't nessacarily the most sophisticated method for getting your steam ID, its the one I find the easiest.

### Running it
Lastly, all you need to do, is open a terminal and run
```
node index.js
```


Here is a concise list of all environment variables:
- `UPDATE_TIME=[number of seconds to update]`
    - **this HAS TO BE over 30 seconds**
- `STEAM_ID=[your_steam_id]`
    - Prevents the program from prompting you for your steam ID

If everything is setup properly, it should look something like this!:<br>
![Alt text](docs/examples/image.png)
![Alt text](docs/examples/image-1.png)
![Alt text](docs/examples/image-2.png)
![Alt text](docs/examples/image-3.png)


## Notes
This project was never meant to be public, but I decided there was no reason not to. Please do report any problems you encounter, and if there are any issues with the documentation, please let me know!

If an image is wider than 1:1, then discord automatically "zooms in" on the image. Sadly, the only high quality image for the game we can get from Steam is very wide, specifically its the title icon. To fix this, all images are funneled through "thumbor"
Currently I am using a self hosted instance of Thumbor for this, which automatically changes the image size so it fits in Discord.
The link to the Thumbor instance can be found here:
https://thumbor-production-0e82.up.railway.app/ 
For getting the right image size, we use this url configuration:
`https://thumbor-production-0e82.up.railway.app/unsafe/fit-in/512x512/filters:fill(transparent)/[Link to the image]`

## To-Do

- [x] Pull images from steam directly
- [x] Show profile picture and username as small Image
- [x] Hide when not playing a game
- [ ] Allow Steam Icon Overrides
- [ ] Add blacklist for certain games that have their own RPC
- [ ] Add config